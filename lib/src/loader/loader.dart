import 'dart:async';

import 'package:dart_common/errors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/src/loader/item_bloc.dart';

typedef LoaderBuilder<T extends ItemDataState> = Widget Function(BuildContext, T);

class LoaderWidget<S extends ItemDataState> extends StatefulWidget {
  final ItemBloc loader;
  final LoaderBuilder<S> builder;
  final void Function(ItemState state)? onState;
  final Widget Function(BuildContext)? loadingBuilder;
  final Widget Function(BuildContext)? errorBuilder;

  const LoaderWidget(
      {Key? key,
      required this.loader,
      required this.builder,
      this.onState,
      this.loadingBuilder,
      this.errorBuilder})
      : super(key: key);

  @override
  State<LoaderWidget<S>> createState() => _LoaderWidgetState<S>();
}

class _LoaderWidgetState<S extends ItemDataState> extends State<LoaderWidget<S>> {
  StreamSubscription<ItemState>? onState;

  @override
  void initState() {
    super.initState();
    if (widget.onState != null) {
      onState = widget.loader.stream().listen(widget.onState!.call);
    }
  }

  @override
  void dispose() {
    onState?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ItemState>(
        initialData: widget.loader.init(),
        stream: widget.loader.stream(),
        builder: (context, snapshot) {
          if (snapshot.data is S) {
            final S state = snapshot.data as S;
            return widget.builder(context, state);
          } else if (snapshot.data is ItemErrorState) {
            final ItemErrorState? state = snapshot.data as ItemErrorState?;
            if (state!.error is ErrorHttp) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                showError(context, state.error as ErrorHttp);
              });
            } else {
              throw state.error;
            }
            return widget.errorBuilder != null ? widget.errorBuilder!(context) : _reload();
          }
          return widget.loadingBuilder != null
              ? widget.loadingBuilder!(context)
              : const Center(child: CircularProgressIndicator());
        });
  }

  Widget _reload() {
    return Center(
        child: IconButton(
            tooltip: 'Reload', icon: const Icon(Icons.sync), onPressed: widget.loader.load));
  }
}
