import 'package:flutter/material.dart';
import 'package:flutter_common/src/utils/colors.dart';

typedef RecordedCallback = void Function(bool recorded);

class RecordButton extends StatefulWidget {
  final Color? selectedColor;
  final Color? unselectedColor;
  final Color? textColor;
  final bool initRecorded;
  final RecordedCallback? onRecordedChanged;
  final int type;
  final String queuedText;
  final String recordText;

  const RecordButton.icon(this.initRecorded,
      {this.onRecordedChanged, this.selectedColor, this.unselectedColor, this.textColor})
      : type = 0,
        queuedText = '',
        recordText = '';

  const RecordButton.button(this.initRecorded,
      {this.onRecordedChanged,
      this.selectedColor,
      this.unselectedColor,
      this.textColor,
      this.queuedText = 'Queued',
      this.recordText = 'Record'})
      : type = 1;

  @override
  _RecordButtonState createState() {
    return _RecordButtonState();
  }
}

class _RecordButtonState extends State<RecordButton> {
  bool _recorded = false;

  @override
  void initState() {
    super.initState();
    _recorded = widget.initRecorded;
  }

  void setRecorded(bool recorded) {
    setState(() {
      if (widget.onRecordedChanged != null) {
        widget.onRecordedChanged!(recorded);
      }
    });
  }

  @override
  void didUpdateWidget(RecordButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    _recorded = widget.initRecorded;
  }

  @override
  Widget build(BuildContext context) {
    final accentColor = widget.selectedColor ?? Theme.of(context).colorScheme.secondary;
    final textColor = widget.textColor ?? themeBrightnessColor(context);

    Widget _iconButton() {
      return IconButton(
          icon: Icon(_recorded ? Icons.access_time : Icons.save),
          color: _recorded ? accentColor : textColor.withOpacity(0.3),
          onPressed: () => setRecorded(!_recorded));
    }

    Widget _outlineButton() {
      return OutlinedButton(
          child: Text(widget.recordText, style: TextStyle(color: textColor)),
          onPressed: () {
            setRecorded(true);
          });
    }

    Widget _flatButton() {
      final textStyle = TextStyle(color: backgroundColorBrightness(accentColor));
      return ElevatedButton(
          child: Text(widget.queuedText, style: textStyle),
          onPressed: () {
            setRecorded(false);
          });
    }

    if (widget.type == 1) {
      if (_recorded) {
        return _flatButton();
      }
      return _outlineButton();
    }
    return _iconButton();
  }
}
