import 'package:flutter/material.dart';

class StateCheckBox extends StatefulWidget {
  final String title;
  final bool init;
  final void Function(bool)? onChanged;

  const StateCheckBox({required this.title, this.init = false, this.onChanged});

  @override
  _CheckBoxState createState() {
    return _CheckBoxState();
  }
}

class _CheckBoxState extends State<StateCheckBox> {
  late bool _value;

  @override
  void initState() {
    super.initState();
    _value = widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
        title: Text(widget.title),
        value: _value,
        onChanged: (value) {
          setState(() {
            _value = value!;
          });
          widget.onChanged?.call(_value);
        });
  }
}
