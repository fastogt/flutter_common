import 'package:flutter/material.dart';

class FlatButtonEx extends StatelessWidget {
  final bool filled;
  final String text;
  final VoidCallback? onPressed;
  final FocusNode? focusNode;
  final ButtonStyle? style;

  const FlatButtonEx.filled({required this.text, this.onPressed, this.focusNode, this.style})
      : filled = true;

  const FlatButtonEx.notFilled({required this.text, this.onPressed, this.focusNode, this.style})
      : filled = false;

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        padding: const EdgeInsets.all(8),
        child: filled
            ? ElevatedButtonEx(text: text, onPressed: onPressed, focusNode: focusNode, style: style)
            : TextButtonEx(text: text, onPressed: onPressed, focusNode: focusNode));
  }
}

class ElevatedButtonEx extends ElevatedButton {
  ElevatedButtonEx(
      {required String text,
      required VoidCallback? onPressed,
      FocusNode? focusNode,
      ButtonStyle? style})
      : super(
          onPressed: onPressed,
          focusNode: focusNode,
          style: style,
          child: Text(text),
        );

  factory ElevatedButtonEx.icon({
    required String text,
    required VoidCallback onPressed,
    FocusNode? focusNode,
    ButtonStyle? style,
  }) {
    return ElevatedButtonEx(text: text, onPressed: onPressed, focusNode: focusNode, style: style);
  }
}

class OutlinedButtonEx extends OutlinedButton {
  OutlinedButtonEx({required String text, required VoidCallback onPressed})
      : super(onPressed: onPressed, child: Text(text));

  factory OutlinedButtonEx.icon(
      {required String text, required IconData icon, required VoidCallback onPressed}) {
    return OutlinedButton.icon(onPressed: onPressed, icon: Icon(icon), label: Text(text))
        as OutlinedButtonEx;
  }
}

class TextButtonEx extends TextButton {
  TextButtonEx({required String text, required VoidCallback? onPressed, FocusNode? focusNode})
      : super(onPressed: onPressed, child: Text(text), focusNode: focusNode);

  factory TextButtonEx.icon(
      {required String text,
      required IconData icon,
      required VoidCallback onPressed,
      FocusNode? focusNode}) {
    return TextButton.icon(
        onPressed: onPressed,
        icon: Icon(icon),
        label: Text(text),
        focusNode: focusNode) as TextButtonEx;
  }
}
