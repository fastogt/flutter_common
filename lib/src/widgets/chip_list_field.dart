import 'package:flutter/material.dart';

class ChipListField extends StatefulWidget {
  final List<String> values;
  final ValueSetter<String> onItemAdded;
  final ValueSetter<int> onItemRemoved;
  final String? hintText;

  const ChipListField(
      {Key? key,
      required this.values,
      required this.onItemAdded,
      required this.onItemRemoved,
      this.hintText})
      : super(key: key);

  @override
  State<ChipListField> createState() => _ChipListFieldState();
}

class _ChipListFieldState extends State<ChipListField> {
  final _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final chips = List.generate(widget.values.length, _chip);
    final field = _field();

    return InputDecorator(
        decoration: InputDecoration(
            label: Text(widget.hintText ?? ''),
            floatingLabelBehavior: FloatingLabelBehavior.always),
        child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 8,
            runSpacing: 8,
            children: chips..add(field)));
  }

  @override
  void didUpdateWidget(covariant ChipListField oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  Widget _chip(int index) {
    return InputChip(
      label: Text(widget.values[index]),
      deleteIcon: const Icon(Icons.remove_circle_outline_rounded, size: 18),
      onDeleted: () => widget.onItemRemoved(index),
    );
  }

  Widget _field() {
    return SizedBox(
        child: TextFormField(
            key: const ValueKey(1),
            decoration: const InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 8),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                disabledBorder: InputBorder.none),
            controller: _textController,
            onFieldSubmitted: _add),
        width: 200);
  }

  void _add(String value) {
    if (value.isEmpty) {
      return;
    }

    _textController.clear();
    widget.onItemAdded(value);
  }
}
