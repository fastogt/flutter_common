import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/src/utils/keys.dart';

KeyEventResult onKey(RawKeyEvent event, KeyEventResult Function(int code) onKey) {
  if (event is RawKeyDownEvent && event.data is RawKeyEventDataAndroid) {
    final RawKeyDownEvent rawKeyDownEvent = event;
    final RawKeyEventDataAndroid rawKeyEventDataAndroid =
        rawKeyDownEvent.data as RawKeyEventDataAndroid;
    final int code = rawKeyEventDataAndroid.keyCode;
    return onKey(code);
  }
  return KeyEventResult.ignored;
}

KeyEventResult onKeyArrows(BuildContext context, RawKeyEvent event,
    {void Function()? onEnter, void Function()? onBack}) {
  return onKey(event, (key) {
    switch (key) {
      case KeyConstants.ENTER:
      case KeyConstants.KEY_CENTER:
        if (onEnter == null) {
          return KeyEventResult.ignored;
        }
        onEnter.call();
        return KeyEventResult.handled;
      case KeyConstants.BACKSPACE:
      case KeyConstants.BACK:
        if (onBack == null) {
          return KeyEventResult.ignored;
        }
        onBack.call();
        return KeyEventResult.handled;
      case KeyConstants.KEY_UP:
        FocusScope.of(context).focusInDirection(TraversalDirection.up);
        return KeyEventResult.handled;
      case KeyConstants.KEY_DOWN:
        FocusScope.of(context).focusInDirection(TraversalDirection.down);
        return KeyEventResult.handled;
      case KeyConstants.KEY_RIGHT:
        FocusScope.of(context).focusInDirection(TraversalDirection.right);
        return KeyEventResult.handled;
      case KeyConstants.KEY_LEFT:
        FocusScope.of(context).focusInDirection(TraversalDirection.left);
        return KeyEventResult.handled;
      default:
        return KeyEventResult.ignored;
    }
  });
}
