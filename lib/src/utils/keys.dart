// Key Constants
class KeyConstants {
  // Common
  static const int KEY_UP = 19;
  static const int KEY_DOWN = 20;
  static const int KEY_LEFT = 21;
  static const int KEY_RIGHT = 22;

  // Remote Controls
  static const int KEY_CENTER = 23;
  static const int MENU = 82;
  static const int INFO = 165;
  static const int BACK = 4;
  static const int PREVIOUS = 89;
  static const int NEXT = 90;
  static const int PAUSE = 85;
  static const int FAVORITE = 174;

  // PC
  static const int ENTER = 66;
  static const int BACKSPACE = 67;
  static const int INFO_KEY = 37;
  static const int MENU_KEY = 41;

  // Additional
  static const int FOCUS_ON_PROGRAMS = 0;
}
