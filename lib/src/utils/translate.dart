import 'package:flutter/material.dart';
import 'package:flutter_common/src/localization/app_localizations.dart';

class TranslationException implements Exception {
  const TranslationException();
}

// throws exception if unable to translate
String translate(BuildContext context, String key) {
  final String? result = AppLocalizations.of(context)!.translate(key);
  if (result == null) {
    throw const TranslationException();
  } else {
    return result;
  }
}

/// returns [key] if unable to translate
String tryTranslate(BuildContext context, String key) {
  final String? result = AppLocalizations.of(context)!.translate(key);
  return result ?? key;
}
