import 'dart:io';

import 'package:flutter/services.dart';

void setStatusBar(bool status) {
  if (status) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
  } else {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  }
}

void exitApp() {
  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
}

void killApp() {
  exit(0);
}
