import 'package:intl/intl.dart';

String dateFromDateTime(DateTime ts) {
  return DateFormat('dd.MM').format(ts);
}

String hmFromDateTime(DateTime ts) {
  return DateFormat('HH:mm').format(ts);
}

String hmsFromDateTime(DateTime ts) {
  return DateFormat('HH:mm:ss').format(ts);
}

String dateFromMsec(int milliseconds) {
  final ts = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  return dateFromDateTime(ts);
}

String hmFromMsec(int milliseconds) {
  final ts = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  return hmFromDateTime(ts);
}

String hmsFromMsec(int milliseconds) {
  final ts = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  return hmsFromDateTime(ts);
}

String msecHumanReadableFormat(int milliseconds) {
  return DateTime.fromMillisecondsSinceEpoch(milliseconds).toString();
}

String formatDurationFromMilliseconds(int value) {
  final duration = Duration(milliseconds: value);
  final String hours = duration.inHours.toString().padLeft(2, '0');
  final String minutes = duration.inMinutes.remainder(60).toString().padLeft(2, '0');
  final String seconds = duration.inSeconds.remainder(60).toString().padLeft(2, '0');
  return "$hours:$minutes:$seconds";
}
