import 'package:dart_common/errors.dart';
import 'package:flutter/material.dart';

Future showError(BuildContext context, IError error) {
  String title = 'Error';
  String text = error.error();
  if (error is ErrorHttp) {
    title = '${error.statusCode} ${error.reason}';
    if (error.isErrorJson()) {
      text = error.errorJson().message;
    }
  }

  final content = Text(text, softWrap: true);
  return showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(title: Text(title), content: content);
      });
}
