import 'package:flutter/material.dart';
import 'package:flutter_common/src/data_table/data_source.dart';
import 'package:flutter_common/src/widgets/no_channels.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DataTableSearchHeader<T> extends StatefulWidget {
  final DataSource<T> source;
  final List<Widget> actions;
  final String title;
  final void Function(String searchText)? preSearchCallback;
  final Color? clearButtonColor;

  const DataTableSearchHeader(
      {required this.source,
      this.clearButtonColor,
      this.preSearchCallback,
      this.actions = const [],
      this.title = 'Search'});

  @override
  _DataTableSearchHeaderState createState() => _DataTableSearchHeaderState();
}

class _DataTableSearchHeaderState extends State<DataTableSearchHeader> {
  DataSource get _dataSource => widget.source;
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _dataSource.addListener(_handleDataSourceChanged);
  }

  @override
  void didUpdateWidget(DataTableSearchHeader oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.source != widget.source) {
      oldWidget.source.removeListener(_handleDataSourceChanged);
      widget.source.addListener(_handleDataSourceChanged);
      _handleDataSourceChanged();
    }
  }

  @override
  void dispose() {
    _dataSource.removeListener(_handleDataSourceChanged);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(mobile: (_) {
      return _mobile();
    }, tablet: (_) {
      return _tablet();
    }, desktop: (_) {
      return _desktop();
    });
  }

  Widget _mobile() {
    return SizedBox(
        height: 40,
        child: ListView(scrollDirection: Axis.horizontal, shrinkWrap: true, children: <Widget>[
          LimitedBox(
            maxWidth: 600,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _searchTextField(),
                ...widget.actions.map((element) {
                  return SizedBox(height: 30, child: element);
                }).toList(),
              ],
            ),
          )
        ]));
  }

  Widget _tablet() {
    return Row(children: [_searchTextField(), const Spacer()] + widget.actions);
  }

  Widget _desktop() {
    return Row(children: [_searchTextField(), const Spacer(flex: 2)] + widget.actions);
  }

  Widget _searchTextField() {
    return Expanded(
        child: ListTile(
            leading: const Icon(Icons.search),
            title: TextField(
                controller: _controller,
                decoration: InputDecoration(hintText: widget.title, border: InputBorder.none),
                onChanged: onSearchTextChanged),
            trailing: _dataSource.searching
                ? IconButton(
                    icon: const Icon(Icons.cancel),
                    color: widget.clearButtonColor ?? Theme.of(context).colorScheme.secondary,
                    onPressed: () {
                      _controller.clear();
                      onSearchTextChanged('');
                    })
                : null));
  }

  void onSearchTextChanged(String text) async {
    if (widget.preSearchCallback != null) {
      widget.preSearchCallback!.call(text);
    }
    _dataSource.search(text);
  }

  void _handleDataSourceChanged() {
    setState(() {});
  }
}

class NoItemsFound extends StatelessWidget {
  final String itemName;

  const NoItemsFound(this.itemName);

  @override
  Widget build(BuildContext context) {
    return NonAvailableBuffer(icon: Icons.search, message: 'No $itemName found');
  }
}
