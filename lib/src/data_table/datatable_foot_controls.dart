import 'package:flutter/material.dart';

class DatatableFootControls extends StatelessWidget {
  final int currentPage, recordsPerPage, countOfRecords;
  final Function(int page) setPage, setLimit;

  const DatatableFootControls({
    super.key,
    required this.currentPage,
    required this.recordsPerPage,
    required this.countOfRecords,
    required this.setPage,
    required this.setLimit,
  });

  @override
  Widget build(BuildContext context) {
    final MaterialLocalizations local = MaterialLocalizations.of(context);
    return DefaultTextStyle(
      style: Theme.of(context).textTheme.bodySmall!,
      child: IconTheme.merge(
        data: const IconThemeData(opacity: 0.54),
        child: SizedBox(
          height: 56.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(local.rowsPerPageTitle),
              const SizedBox(width: 14),
              _limitDropList(context),
              const SizedBox(width: 32),
              Text(
                  "${1 + currentPage * recordsPerPage}-${(currentPage * recordsPerPage) + recordsPerPage} of $countOfRecords"),
              const SizedBox(width: 32),
              _switchButton(
                  Icons.chevron_left, currentPage > 0 ? () => setPage(currentPage - 1) : null),
              _pagesDropList(context),
              _switchButton(
                  Icons.chevron_right,
                  currentPage < (countOfRecords / recordsPerPage).ceil() - 1
                      ? () => setPage(currentPage + 1)
                      : null),
              const SizedBox(width: 14),
            ],
          ),
        ),
      ),
    );
  }

  Widget _limitDropList(BuildContext context) {
    final list = [10, 20, 50, 100];
    return DropdownButtonHideUnderline(
      child: DropdownButton<int>(
        focusColor: Colors.transparent,
        items: list.map((value) {
          return _dropRecordsMenuItem(value);
        }).toList(),
        selectedItemBuilder: (_) {
          return list.map((value) {
            return Center(child: Text('$value'));
          }).toList();
        },
        value: recordsPerPage,
        onChanged: (limit) => setLimit(limit ?? 10),
        style: Theme.of(context).textTheme.bodySmall,
      ),
    );
  }

  DropdownMenuItem<int> _dropRecordsMenuItem(int value) {
    return DropdownMenuItem(
      child: Text('$value'),
      value: value,
    );
  }

  Widget _pagesDropList(BuildContext context) {
    final maximumPage = (countOfRecords / recordsPerPage).ceil() - 1;
    final list = [
      if (currentPage > 1) 0,
      if (currentPage != 0) currentPage - 1,
      currentPage,
      if (currentPage != maximumPage) currentPage + 1,
      if (currentPage < maximumPage - 1) maximumPage,
    ];
    return DropdownButtonHideUnderline(
      child: DropdownButton<int>(
        focusColor: Colors.transparent,
        items: list.map((value) {
          return _dropPagesMenuItem(value);
        }).toList(),
        selectedItemBuilder: (_) {
          return list.map((value) {
            return Center(
              child: Text(
                '${value + 1}',
              ),
            );
          }).toList();
        },
        value: currentPage,
        onChanged: (page) => setPage(page ?? 0),
        style: Theme.of(context).textTheme.bodySmall,
      ),
    );
  }

  DropdownMenuItem<int> _dropPagesMenuItem(int value) {
    return DropdownMenuItem(
      child: Text('${value + 1}'),
      value: value,
    );
  }

  Widget _switchButton(IconData icon, Function()? function) {
    return IconButton(
      icon: Icon(icon),
      color: function != null ? Colors.grey[800] : null,
      padding: EdgeInsets.zero,
      onPressed: function,
    );
  }
}
