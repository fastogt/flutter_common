import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class DataLayout<T> extends StatelessWidget {
  final DataSource<T> dataSource;
  final Widget header;
  final List<Widget> Function(T) singleItemActions;
  final List<Widget> Function(List<T>) multipleItemActions;
  final bool canScrollTable;
  final String? title;
  final Color? usualColor;
  final Color? selectedColor;
  final bool includeHead;
  final Color? selectedTextColor;

  const DataLayout(
      {required this.dataSource,
      required this.header,
      required this.singleItemActions,
      required this.multipleItemActions,
      bool? canScrollTable,
      this.usualColor,
      this.selectedColor,
      this.includeHead = true,
      this.selectedTextColor})
      : canScrollTable = canScrollTable ?? false,
        title = null;

  const DataLayout.withHeader(
      {required this.dataSource,
      required this.header,
      required this.title,
      required this.singleItemActions,
      required this.multipleItemActions,
      bool? canScrollTable,
      this.usualColor,
      this.selectedColor,
      this.includeHead = true,
      this.selectedTextColor})
      : canScrollTable = canScrollTable ?? false;

  @override
  Widget build(BuildContext context) {
    if (canScrollTable) {
      return ListView(children: [_content()]);
    } else {
      return _content();
    }
  }

  Widget _content() {
    return title == null ? _table() : _withTitle();
  }

  Widget _withTitle() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_title(), _table()]);
  }

  Widget _table() {
    return DataTableEx(dataSource, header, _actions, null, null, usualColor, selectedColor,
        includeHead, null, selectedColor);
  }

  Widget _title() {
    final titleToUtf = AppLocalizations.toUtf8(title!);
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(titleToUtf, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
  }

  List<Widget> _actions() {
    if (dataSource.selectedRowCount == 1) {
      return singleItemActions(dataSource.selectedItems().first);
    } else if (dataSource.selectedRowCount > 1) {
      return multipleItemActions(dataSource.selectedItems());
    }
    return [];
  }
}
