import 'package:flutter/material.dart';
import 'package:flutter_common/src/data_table/layouts.dart';
import 'package:flutter_common/src/data_table/table.dart';
import 'package:flutter_common/src/localization/app_localizations.dart';

class PaginationDataLayout<T> extends DataLayout<T> {
  final Widget? footControls;
  final int? currentPage, rowsPerPage;
  final Color? selectedTextItemsColors;

  const PaginationDataLayout({
    required super.dataSource,
    required super.header,
    required super.singleItemActions,
    required super.multipleItemActions,
    this.selectedTextItemsColors,
    this.footControls,
    this.currentPage,
    this.rowsPerPage,
    super.usualColor,
    super.selectedColor,
  }) : super();

  @override
  Widget build(BuildContext context) {
    if (canScrollTable) {
      return ListView(children: [_content()]);
    } else {
      return _content();
    }
  }

  Widget _content() {
    return title == null ? _table() : _withTitle();
  }

  Widget _withTitle() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_title(), _table()]);
  }

  Widget _table() {
    return DataTableEx(dataSource, header, _actions, currentPage, rowsPerPage, usualColor,
        selectedColor, includeHead, footControls, selectedTextItemsColors);
  }

  Widget _title() {
    final titleToUtf = AppLocalizations.toUtf8(title!);
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(titleToUtf, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
  }

  List<Widget> _actions() {
    if (dataSource.selectedRowCount == 1) {
      return singleItemActions(dataSource.selectedItems().first);
    } else if (dataSource.selectedRowCount > 1) {
      return multipleItemActions(dataSource.selectedItems());
    }
    return [];
  }
}
