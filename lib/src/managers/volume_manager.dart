import 'dart:async';

import 'package:flutter_volume_controller/flutter_volume_controller.dart';

class VolumeManager {
  static VolumeManager? _instance;
  double _maxVol = 1.0;
  double _currentVol = 0.5;
  late StreamSubscription<double> _volumeSubscription;

  static Future<VolumeManager> getInstance() {
    if (_instance == null) {
      _instance = VolumeManager();
      return _instance!._initVolumes().then((_) {
        _instance!._initVolumeListener();
        return _instance!;
      });
    }
    return Future.value(_instance!);
  }

  double maxVolume() {
    return _maxVol;
  }

  double currentVolume() {
    return _currentVol;
  }

  Future<void> setVolume(double volume) {
    _instance!._currentVol = volume;
    return FlutterVolumeController.setVolume(volume);
  }

  Future<void> _initVolumes() {
    return FlutterVolumeController.getVolume().then((volume) {
      if (volume != null) {
        _currentVol = volume;
      }
    });
  }

  void _initVolumeListener() {
    _volumeSubscription = FlutterVolumeController.addListener((volume) {
      _currentVol = volume;
    });
  }

  void dispose() {
    _volumeSubscription.cancel();
  }

  Future<AudioStream?> get iosAudioSessionCategory async {
    return FlutterVolumeController.getAndroidAudioStream();
  }
}
