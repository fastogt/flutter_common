import 'dart:async';

import 'package:ntp/ntp.dart';

class TimeManager {
  static TimeManager? _instance;
  int _difference = 0;

  static Future<TimeManager> getInstance() async {
    if (_instance == null) {
      _instance = TimeManager();
      await _instance!._init();
    }
    return _instance!;
  }

  int differenceMSec() {
    return _difference;
  }

  int realTimeMSec() {
    final now = DateTime.now();
    return now.millisecondsSinceEpoch + _difference;
  }

  // private:
  Future<void> _init() async {
    try {
      final DateTime ntp = await NTP.now();
      final now = DateTime.now();
      _difference = ntp.millisecondsSinceEpoch - now.millisecondsSinceEpoch;
    } catch (e) {
      _difference = 0;
    }
  }
}
