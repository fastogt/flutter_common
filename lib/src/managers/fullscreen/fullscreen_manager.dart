import 'fullscreen_delegate_base.dart';
import 'fullscreen_delegate_mobile.dart' if (dart.library.js) 'fullscreen_delegate_web.dart';

class FullscreenManager {
  static FullscreenManager? _instance;

  static FullscreenManager get instance {
    _instance ??= FullscreenManager._();
    return _instance!;
  }

  FullscreenManager._();

  final FullscreenDelegateBase _delegate = FullscreenDelegate();

  bool _enabled = false;

  bool get enabled => _enabled;

  Future<void> setFullscreen(bool value) async {
    await _delegate.setFullscreen(value);
    _enabled = value;
  }

  Future<void> toggleFullsreen() async {
    await setFullscreen(!_enabled);
  }
}
