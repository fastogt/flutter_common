import 'dart:async';
import 'dart:io' show Platform;
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:fastotv_device_info/device.dart';
import 'package:fastotv_device_info/devices.dart';
import 'package:flutter/foundation.dart';

class RuntimeDevice {
  static RuntimeDevice? _instance;
  AndroidDeviceInfo? _android;
  IosDeviceInfo? _ios;
  WebBrowserInfo? _web;
  late bool _hasTouch;
  bool _registeredInOurDB = false;

  static Future<RuntimeDevice> getInstance() async {
    if (_instance == null) {
      _instance = RuntimeDevice();
      await _instance!._init();
    }
    return _instance!;
  }

  String arch = 'unknown';
  String cpuBrand = 'unknown';
  String version = 'unknown';
  Device? _currentDevice;

  AndroidDeviceInfo? get androidDetails {
    return _android;
  }

  bool isRegisteredInOurDB() {
    return _registeredInOurDB;
  }

  IosDeviceInfo? get iosDetails {
    return _ios;
  }

  WebBrowserInfo? get webDetails {
    return _web;
  }

  static List<String> get platforms => ['web', 'android', 'ios'];

  String get platform {
    return os.toLowerCase();
  }

  String get os {
    if (kIsWeb) {
      return 'Web';
    }
    if (Platform.isAndroid) {
      return 'Android';
    } else if (Platform.isIOS) {
      return 'iOS';
    } else if (Platform.isLinux) {
      return 'Linux';
    } else if (Platform.isWindows) {
      return 'Windows';
    } else if (Platform.isMacOS) {
      return 'MacOSX';
    }
    return 'Unknown';
  }

  bool get hasTouch {
    return _hasTouch;
  }

  Future<bool> get futureTouch {
    if (kIsWeb) {
      return Future<bool>.value(_hasTouch);
    }
    return _currentDevice!.hasTouch();
  }

  String get name {
    if (kIsWeb) {
      return 'Browser';
    }
    return _currentDevice!.name;
  }

  Future<void> _init() async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (kIsWeb) {
      _web = await deviceInfoPlugin.webBrowserInfo;
      version = _web!.appCodeName!;
      arch = _web!.appVersion!;
      cpuBrand = _web!.appName!;
      _hasTouch = false;
      return;
    }

    if (Platform.isIOS) {
      _ios = await deviceInfoPlugin.iosInfo;
      arch = _ios!.utsname.machine;
      cpuBrand = APPLE_BRAND;
      version = _ios!.systemVersion;
    } else if (Platform.isAndroid) {
      _android = await deviceInfoPlugin.androidInfo;
      arch = _android!.model;
      cpuBrand = _android!.manufacturer;
      version = _android!.version.sdkInt.toString();
    }
    if (Devices.all.containsKey(cpuBrand)) {
      final devices = Devices.all[cpuBrand]!;
      for (final Device device in devices) {
        if (device.model == arch) {
          _currentDevice = device;
          _registeredInOurDB = true;
          break;
        }
      }
    }

    if (_currentDevice == null) {
      if (Platform.isIOS) {
        _currentDevice = IOSDevice(name: arch, model: arch);
      } else if (Platform.isAndroid) {
        _currentDevice = AndroidDevice(name: arch, model: arch);
      } else if (Platform.isWindows) {
        _currentDevice = WindowsDevice(name: arch, model: arch);
      } else if (Platform.isLinux) {
        _currentDevice = LinuxDevice(name: arch, model: arch);
      } else if (Platform.isMacOS) {
        _currentDevice = MacOSXDevice(name: arch, model: arch);
      } else {
        assert(false);
      }
    }
    _hasTouch = await _currentDevice!.hasTouch();
  }
}
